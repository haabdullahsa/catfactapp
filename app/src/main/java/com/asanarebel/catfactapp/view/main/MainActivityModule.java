package com.asanarebel.catfactapp.view.main;

import com.asanarebel.catfactapp.provider.interactor.CatFactsInteractor;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    public MainActivityContract.MainPresenter provideMainActivityPresenterImpl(CatFactsInteractor catFactsInteractor) {
        return new MainPresenterImpl(catFactsInteractor);
    }
}
