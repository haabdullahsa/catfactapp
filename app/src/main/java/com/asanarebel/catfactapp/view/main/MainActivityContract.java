package com.asanarebel.catfactapp.view.main;

import android.widget.SeekBar;

import com.asanarebel.catfactapp.provider.model.response.Data;

import java.util.List;

import butterknife.Unbinder;

public interface MainActivityContract {

    interface MainView {
        void fillList(List<Data> dataList);

        void showNoData();
    }

    interface MainPresenter {
        void attachView(Unbinder unbinder, MainView mainView);

        void detachView();

        void getRandomCatFactList(int maxLength);

        void resetDatas();

        void onLoadMore(SeekBar seekBar);
    }

}
