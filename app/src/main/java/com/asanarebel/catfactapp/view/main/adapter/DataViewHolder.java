package com.asanarebel.catfactapp.view.main.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.asanarebel.catfactapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DataViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textview_fact)
    TextView textViewFact;
    private Unbinder unbinder;

    public DataViewHolder(View itemView) {
        super(itemView);
        unbinder = ButterKnife.bind(this, itemView);
    }

    protected void unBind() {
        unbinder.unbind();
        unbinder = null;
    }

}
