package com.asanarebel.catfactapp.view.main;

import android.widget.SeekBar;

import com.asanarebel.catfactapp.core.CatFactApp;
import com.asanarebel.catfactapp.provider.enums.AppConstants;
import com.asanarebel.catfactapp.provider.interactor.CatFactsInteractor;
import com.asanarebel.catfactapp.provider.manager.SharedPreferencesManager;
import com.asanarebel.catfactapp.provider.model.response.BaseResponse;
import com.asanarebel.catfactapp.provider.model.response.Data;

import java.util.List;

import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainPresenterImpl implements MainActivityContract.MainPresenter {

    private CompositeDisposable compositeDisposable;

    private static final int LIMIT = 20;

    private int page = 1;

    private boolean isLoadDataFinished = false;

    private Unbinder unbinder;

    private CatFactsInteractor catFactsInteractor;

    private MainActivityContract.MainView mainView;

    private SharedPreferencesManager sharedPreferencesManager;

    MainPresenterImpl(CatFactsInteractor catFactsInteractor) {
        this.catFactsInteractor = catFactsInteractor;
        this.sharedPreferencesManager = CatFactApp.getInstance()
                .getAppComponent().sharedPreferencesManager();
    }

    @Override
    public void attachView(Unbinder unbinder, MainActivityContract.MainView mainView) {
        if (mainView != null) {
            this.mainView = mainView;
            this.unbinder = unbinder;
            compositeDisposable = new CompositeDisposable();
        }
    }

    @Override
    public void detachView() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        mainView = null;
    }

    @Override
    public void getRandomCatFactList(int maxLength) {
        compositeDisposable.add(catFactsInteractor.getListAccordingToParameters(maxLength, LIMIT, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleDataResponse, error -> mainView.showNoData()));
    }

    private void handleDataResponse(List<Data> datas) {
        if (datas != null && !datas.isEmpty()) {
            mainView.fillList(datas);
            BaseResponse baseResponse = (BaseResponse) sharedPreferencesManager
                    .getRecord(AppConstants.CACHED_RESPONSE, BaseResponse.class);
            if (baseResponse.getCurrentPage() == baseResponse.getLastPage()) {
                isLoadDataFinished = true;
            }
            page++;
        } else {
            isLoadDataFinished = true;
            mainView.showNoData();
        }
    }

    @Override
    public void resetDatas() {
        isLoadDataFinished = false;
        page = 1;
    }

    @Override
    public void onLoadMore(SeekBar seekBar) {
        if (!isLoadDataFinished) {
            getRandomCatFactList(seekBar.getProgress());
        }
    }
}
