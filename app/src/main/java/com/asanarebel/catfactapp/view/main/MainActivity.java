package com.asanarebel.catfactapp.view.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.asanarebel.catfactapp.R;
import com.asanarebel.catfactapp.core.CatFactApp;
import com.asanarebel.catfactapp.provider.enums.AppConstants;
import com.asanarebel.catfactapp.provider.enums.CommonValues;
import com.asanarebel.catfactapp.provider.model.response.Data;
import com.asanarebel.catfactapp.view.main.adapter.CatDataListAdapter;
import com.asanarebel.catfactapp.view.main.listener.ScrollDatasListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainActivityContract.MainView, SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.recyclerview_catfact_list)
    RecyclerView recyclerViewCatFactList;

    @BindView(R.id.seekbar_max_length)
    SeekBar seekbarMaxLength;

    @BindView(R.id.text_max_length)
    TextView textMaxLength;

    @BindView(R.id.text_empty)
    TextView textEmpty;

    @Inject
    MainActivityContract.MainPresenter mainPresenter;

    private CatDataListAdapter catDataListAdapter;
    private ProgressDialog dialog;

    private boolean isInit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initDagger();
        mainPresenter.attachView(ButterKnife.bind(this), this);
        initProgress();
        init();
        seekbarMaxLength.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        textMaxLength.setText(CommonValues.MAX_LENGTH_TEXT + seekBar.getProgress());
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isInit = true;
        mainPresenter.resetDatas();
        getDatas();
    }

    @Override
    public void fillList(List<Data> datas) {
        textEmpty.setVisibility(View.GONE);
        catDataListAdapter.updateDatas(datas, isInit);
        dialog.dismiss();
    }

    @Override
    public void showNoData() {
        textEmpty.setVisibility(View.VISIBLE);
        catDataListAdapter.updateDatas(null, isInit);
        dialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.detachView();
        catDataListAdapter.unbindTracking();
    }

    private void initDagger() {
        DaggerMainActivityComponent.builder()
                .appComponent((CatFactApp.getInstance().getAppComponent()))
                .mainActivityModule(new MainActivityModule())
                .build().inject(this);
    }

    private void init() {
        isInit = true;
        textMaxLength.setText(CommonValues.MAX_LENGTH_TEXT + seekbarMaxLength.getProgress());
        mainPresenter.resetDatas();
        catDataListAdapter = new CatDataListAdapter(this, onItemClickListener);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewCatFactList.setHasFixedSize(true);
        recyclerViewCatFactList.setLayoutManager(linearLayoutManager);
        recyclerViewCatFactList.setAdapter(catDataListAdapter);
        recyclerViewCatFactList.addOnScrollListener(scrollDatasListener);
        getDatas();
    }

    private void initProgress() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(CommonValues.LOADING);
        dialog.setCancelable(false);
    }

    private CatDataListAdapter.OnItemClickListener onItemClickListener = data -> {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, data.getFact());
        sendIntent.setType(AppConstants.INTENT_TYPE);
        startActivity(Intent.createChooser(sendIntent, CommonValues.CHOOSE_AND_SHARE));
    };

    private ScrollDatasListener scrollDatasListener = new ScrollDatasListener() {
        @Override
        public void onLoadMore() {
            isInit = false;
            mainPresenter.onLoadMore(seekbarMaxLength);
        }
    };

    private void getDatas() {
        dialog.show();
        mainPresenter.getRandomCatFactList(seekbarMaxLength.getProgress());
    }

}
