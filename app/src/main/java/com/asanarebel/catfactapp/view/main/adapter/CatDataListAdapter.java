package com.asanarebel.catfactapp.view.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asanarebel.catfactapp.R;
import com.asanarebel.catfactapp.provider.model.response.Data;

import java.util.List;

public class CatDataListAdapter extends RecyclerView.Adapter<DataViewHolder> {

    private List<Data> dataList;
    private Context context;
    private DataViewHolder dataViewHolder;
    private OnItemClickListener onItemClickListener;

    public CatDataListAdapter(Context context, OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.context = context;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        dataViewHolder = new DataViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_cat_data, parent, false));
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.textViewFact.setText(dataList.get(position).getFact());
        click(dataList.get(position), holder.itemView);
    }

    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }

    public void updateDatas(List<Data> dataList, boolean isInit) {
        if (this.dataList == null || isInit) {
            this.dataList = dataList;
            notifyDataSetChanged();
        } else {
            int pos = this.dataList.size();
            this.dataList.addAll(dataList);
            notifyItemInserted(pos + 1);
        }
    }

    public void unbindTracking() {
        dataViewHolder.unBind();
    }

    public interface OnItemClickListener {
        void onItemClick(Data data);
    }

    private void click(Data data, View itemView) {
        itemView.setOnClickListener(view -> onItemClickListener.onItemClick(data));
    }
}
