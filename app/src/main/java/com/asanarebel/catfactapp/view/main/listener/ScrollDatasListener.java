package com.asanarebel.catfactapp.view.main.listener;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class ScrollDatasListener extends RecyclerView.OnScrollListener {

    private int lastVisibleItemPos;
    private int totalCount;

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        lastVisibleItemPos = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
        totalCount = recyclerView.getAdapter().getItemCount();

        if (lastVisibleItemPos + 1 == totalCount) {
            onLoadMore();
        }
    }

    public abstract void onLoadMore();
}
