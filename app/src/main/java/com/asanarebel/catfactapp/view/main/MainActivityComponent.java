package com.asanarebel.catfactapp.view.main;

import com.asanarebel.catfactapp.core.AppComponent;
import com.asanarebel.catfactapp.provider.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class, modules = MainActivityModule.class)
public interface MainActivityComponent {
    void inject(MainActivity mainActivity);
}
