package com.asanarebel.catfactapp.core;

import com.asanarebel.catfactapp.provider.interactor.CatFactsInteractor;
import com.asanarebel.catfactapp.provider.manager.SharedPreferencesManager;
import com.asanarebel.catfactapp.provider.network.NetworkModule;
import com.asanarebel.catfactapp.provider.scope.ApplicationScope;

import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {

    SharedPreferencesManager sharedPreferencesManager();

    CatFactsInteractor catFactsInteractor();

}
