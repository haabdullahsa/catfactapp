package com.asanarebel.catfactapp.core;

import android.content.Context;
import android.content.SharedPreferences;

import com.asanarebel.catfactapp.provider.enums.AppConstants;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @Inject
    public SharedPreferences provideSharedPreferences() {
        return context.getSharedPreferences(AppConstants.APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
    }

}
