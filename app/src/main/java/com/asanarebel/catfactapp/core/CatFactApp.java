package com.asanarebel.catfactapp.core;

import android.app.Application;

import com.asanarebel.catfactapp.provider.network.NetworkModule;

public class CatFactApp extends Application {

    private static CatFactApp instance = null;
    private AppComponent appComponent;

    public static CatFactApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (instance == null) {
            instance = this;
        }
        initDagger();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private void initDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }
}
