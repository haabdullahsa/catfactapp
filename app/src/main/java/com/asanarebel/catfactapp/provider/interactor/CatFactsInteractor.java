package com.asanarebel.catfactapp.provider.interactor;

import com.asanarebel.catfactapp.provider.enums.AppConstants;
import com.asanarebel.catfactapp.provider.manager.SharedPreferencesManager;
import com.asanarebel.catfactapp.provider.model.response.Data;
import com.asanarebel.catfactapp.provider.network.service.CatFactService;
import com.asanarebel.catfactapp.provider.scope.ApplicationScope;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;


@ApplicationScope
public class CatFactsInteractor {

    private SharedPreferencesManager sharedPreferencesManager;
    private CatFactService catFactService;

    @Inject
    CatFactsInteractor(SharedPreferencesManager sharedPreferencesManager, CatFactService catFactService) {
        this.sharedPreferencesManager = sharedPreferencesManager;
        this.catFactService = catFactService;
    }

    public Observable<List<Data>> getListAccordingToParameters(int maxLength, int limit, int page) {
        return catFactService.getCatFactList(maxLength, limit, page)
                .doOnNext(baseResponse -> {
                    if (baseResponse != null && !baseResponse.getDataList().isEmpty()) {
                        sharedPreferencesManager.setRecord(AppConstants.CACHED_RESPONSE, baseResponse);
                    }
                }).switchMap(response -> Observable.just(response.getDataList()));
    }

}
