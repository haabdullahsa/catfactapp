package com.asanarebel.catfactapp.provider.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@StringDef({CommonValues.EMPTY_TEXT, CommonValues.MAX_LENGTH_TEXT,
        CommonValues.LOADING, CommonValues.CHOOSE_AND_SHARE})
@Retention(RetentionPolicy.SOURCE)
public @interface CommonValues {
    String EMPTY_TEXT = "";
    String MAX_LENGTH_TEXT = "Max Length: ";
    String LOADING = "Loading...";
    String CHOOSE_AND_SHARE = "Choose and Share";

}
