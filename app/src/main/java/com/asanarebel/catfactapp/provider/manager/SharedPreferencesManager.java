package com.asanarebel.catfactapp.provider.manager;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.asanarebel.catfactapp.provider.scope.ApplicationScope;
import com.google.gson.Gson;

import java.lang.reflect.Type;

import javax.inject.Inject;

@ApplicationScope
public class SharedPreferencesManager {

    private SharedPreferences sharedPreferences;
    private Gson gson;

    @Inject
    SharedPreferencesManager(SharedPreferences sharedPreferences, Gson gson) {
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
    }

    public void setRecord(String name, Object o) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(name, gson.toJson(o));
        editor.apply();
    }

    public Object getRecord(String name, Type type) {
        String jsonValue = sharedPreferences.getString(name, null);
        return !TextUtils.isEmpty(jsonValue) ? gson.fromJson(jsonValue, type) : null;
    }

}
