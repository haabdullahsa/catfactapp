package com.asanarebel.catfactapp.provider.model.response;


import android.os.Parcel;
import android.os.Parcelable;

public class Data implements Parcelable {

    private String fact;

    private String length;

    public String getFact() {
        return fact;
    }

    public void setFact(String fact) {
        this.fact = fact;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(fact);
        parcel.writeString(length);
    }

    protected Data(Parcel in) {
        fact = in.readString();
        length = in.readString();
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    @Override
    public String toString() {
        return "Data{" +
                "fact='" + fact + '\'' +
                ", length='" + length + '\'' +
                '}';
    }
}
