package com.asanarebel.catfactapp.provider.scope;

import javax.inject.Scope;


@Scope
public @interface ApplicationScope {
}
