package com.asanarebel.catfactapp.provider.network.service;

import com.asanarebel.catfactapp.provider.model.response.BaseResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CatFactService {
    @GET("facts")
    Observable<BaseResponse> getCatFactList(@Query("max_length") int maxLength,
                                            @Query("limit") int limit,
                                            @Query("page") int page);
}
