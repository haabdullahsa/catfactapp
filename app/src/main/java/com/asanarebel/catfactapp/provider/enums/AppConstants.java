package com.asanarebel.catfactapp.provider.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({AppConstants.HTTPS_BASE_URL, AppConstants.CACHED_RESPONSE,
        AppConstants.APPLICATION_PREFERENCES, AppConstants.INTENT_TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface AppConstants {
    String HTTPS_BASE_URL = "https://catfact.ninja";
    String CACHED_RESPONSE = "cached_response";
    String APPLICATION_PREFERENCES = "application_preferences";
    String INTENT_TYPE = "text/plain";
}
