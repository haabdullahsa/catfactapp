package com.asanarebel.catfactapp.provider.network;

import com.asanarebel.catfactapp.provider.enums.AppConstants;
import com.asanarebel.catfactapp.provider.network.service.CatFactService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    public Gson provideGson() {
        return new GsonBuilder().setLenient().create();
    }

    @Provides
    public Retrofit provideRetrofit(Gson gson) {
        return new Retrofit.Builder().baseUrl(AppConstants.HTTPS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(new OkHttpClient())
                .build();
    }

    @Provides
    public CatFactService provideCatFactService(Retrofit retrofit) {
        return retrofit.create(CatFactService.class);
    }

}
